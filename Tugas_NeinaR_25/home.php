<?php 
session_start();

include 'koneksi.php';

if (!isset($_SESSION["login"])) {
	header("Location: index.php");

	exit;
 }

if (isset($_GET["pesan"])) {
	if ($_GET["pesan"] === "berhasil_hapus") {
		$pesan = "Berhasil menghapus data";
		$warna = "success";
	}
	if ($_GET["pesan"] === "berhasil_ubah") {
		$pesan = "Berhasil mengubah data";
		$warna = "success";
	}
	if ($_GET["pesan"] === "gagal_hapus") {
		$pesan = "Gagal menghapus data";
		$warna = "danger";
	}
}

$username = $_SESSION["username"];

$sql = "SELECT * FROM users";
$result = mysqli_query($koneksi, $sql);

// Fetch all
$hasil = mysqli_fetch_all($result, MYSQLI_ASSOC);

 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-success">
  		<div class="container-fluid">
		    <a class="btn btn-primary" href="logout.php">Logout</a>
		    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
		      <span class="navbar-toggler-icon"></span>
		    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
      </div>
    </div>
  </div>
</nav>
	<div class="container">
		<h1>Data User</h1>
		<a class="btn btn-secondary" href="tambah.php">Tambah Data</a>
		<?php 
		if (isset($pesan)) {
		 ?>
		 <div class="alert alert-<?=$warna ?>" role="alert">
		  	<?=$pesan; ?>
		</div>
		<?php

		 } 
		 ?>
		
		<table class="table table-bordered">
			<thead>
				<tr>
					<td>No.</td>
					<td>Nama</td>
					<td>Username</td>
					<td>Email</td>
					<td>Avatar</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody>
				<?php 
				foreach ($hasil as $key => $user_data) {
				 ?>
				<tr>
					<td><?= $key + 1 ?></td>
					<td><?= $user_data["nama"] ?></td>
					<td><?= $user_data["username"] ?></td>
					<td><?= $user_data["email"] ?></td>
					<td><?= $user_data["avatar"]?></td>
					<td>
						<a class="btn btn-success" href="form_ubah.php?id=<?= $user_data['id']?>">Change</a>
						<a class="btn btn-danger" href="proses_hapus.php?id=<?= $user_data['id']?>">Delete</a>
					</td>
				</tr>
				<?php 
				}
				 ?>
			</tbody>
			<tfoot>
				
			</tfoot>
		</table>
	</div>
</body>
		<div class="footer footer-light bg-success">
      	<h6 class="copy" align="center"><p><?php echo date("d-l-M-y H:i:s", time() + (60*60*6));?></p></h6>
</div>
</html>